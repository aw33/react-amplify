import { API, graphqlOperation } from "aws-amplify";
import { useState } from "react";
import { createTodo } from "../graphql/mutations";

const AddItem = () => {
  const [item, setItem] = useState<string>("");

  const saveItem = async () => {
    try {
      await API.graphql(
        graphqlOperation(createTodo, {
          input: {
            name: item,
            description: "My todo",
          },
        })
      );

      console.log("Item added");
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <h1>TO-DO list</h1>
      <input
        type="text"
        value={item}
        onChange={(e) => setItem(e.target.value)}
      />

      <button onClick={saveItem}>Save</button>
    </div>
  );
};

export default AddItem;
