import React, { useEffect, useRef, useState } from "react";
import { API, graphqlOperation } from "aws-amplify";

import { listTodos } from "../graphql/queries";
import { GraphQLResult } from "@aws-amplify/api-graphql";
import {
  DeleteTodoInput,
  Todo,
  UpdateTodoInput,
  ListTodosQueryVariables,
  ListTodosQuery,
} from "../api/graphql.api";
import {
  onCreateTodo,
  onDeleteTodo,
  onUpdateTodo,
} from "../graphql/subscriptions";
import { deleteTodo, updateTodo } from "../graphql/mutations";

type MyGraphQLResult = GraphQLResult<ListTodosQuery>;

const LIMIT = 3;

const ListItems = () => {
  const [list, setList] = useState<Todo[]>([]);
  const [tokens, setTokens] = useState<Array<string | null | undefined>>([
    null,
  ]);
  const [currentPage, setCurrentPage] = useState<number>(0);

  const deleteTodoHandler = (id: string) => {
    (
      API.graphql(
        graphqlOperation(deleteTodo, {
          input: {
            id: id,
          } as DeleteTodoInput,
        })
      ) as Promise<GraphQLResult>
    ).then((res) => {
      console.log("Delete res", res);
    });
  };

  const updateTodoHandler = (todo: Todo) => {
    (
      API.graphql(
        graphqlOperation(updateTodo, {
          input: {
            id: todo.id,
            name: todo.name,
          } as UpdateTodoInput,
        })
      ) as Promise<any>
    ).then(() => {
      fetchTodos();
    });
  };

  const fetchTodos = () => {
    (
      API.graphql(
        graphqlOperation(listTodos, {
          limit: LIMIT,
          nextToken: tokens[currentPage],
        } as ListTodosQueryVariables)
      ) as Promise<MyGraphQLResult>
    ).then((res) => {
      if (typeof res.data?.listTodos?.nextToken === "string") {
        setTokens((prev) => {
          const prevTokens = [...prev];
          prevTokens[currentPage + 1] = res.data?.listTodos?.nextToken;
          return prevTokens;
        });
      }
      if (res.data?.listTodos?.items) {
        setList(res.data.listTodos.items as Todo[]);
      }
    });
  };

  const todoSubscription = useRef<any[]>([]);
  function subscribe() {
    todoSubscription.current?.push(
      (API.graphql(graphqlOperation(onCreateTodo)) as any).subscribe(
        (res: any) => {
          fetchTodos();
        }
      )
    );

    todoSubscription.current?.push(
      (API.graphql(graphqlOperation(onDeleteTodo)) as any).subscribe(
        (res: any) => {
          fetchTodos();
        }
      )
    );

    todoSubscription.current?.push(
      (API.graphql(graphqlOperation(onUpdateTodo)) as any).subscribe(
        (res: any) => {
          fetchTodos();
        }
      )
    );
  }

  useEffect(() => {
    subscribe();

    return () => {
      todoSubscription.current.forEach((sub) => sub.unsubsribe());
    };
  }, []);

  useEffect(() => {
    fetchTodos();
  }, [currentPage]);

  const goToNextPage = () => {
    if (!!tokens[currentPage + 1]) {
      setCurrentPage((prev) => prev + 1);
    }
  };

  const goToPrevPage = () => {
    if (currentPage > 0) {
      setCurrentPage((prev) => prev - 1);
    }
  };

  useEffect(() => {
    console.log("TOKENS", tokens);
    console.log("CURRENT_PAGE", currentPage);
  }, [tokens, currentPage]);

  return (
    <div>
      <ul>
        {list.map((todoItem) => (
          <Item
            key={todoItem.id}
            item={todoItem}
            onDeleteTodo={deleteTodoHandler}
            onUpdateTodo={updateTodoHandler}
          />
        ))}

        <button onClick={goToPrevPage}>{"<--"}</button>
        <button onClick={goToNextPage}>{"-->"}</button>
      </ul>
    </div>
  );
};

const Item: React.FC<{
  item: Todo;
  onDeleteTodo: Function;
  onUpdateTodo: Function;
}> = ({ item, onDeleteTodo, onUpdateTodo }) => {
  const [todoToUpdate, setTodoToUpdate] = useState<Todo | null>(null);
  const [newName, setNewName] = useState("");

  const initUpdateTodo = (itemToUpdate: Todo) => {
    setNewName(itemToUpdate.name);
    setTodoToUpdate(itemToUpdate);
  };

  const submitUpdateTodo = () => {
    onUpdateTodo({ ...todoToUpdate, name: newName });
    item.name = newName;
    setTodoToUpdate(null);
  };

  return (
    <li key={item.id}>
      {todoToUpdate ? (
        <>
          <input value={newName} onChange={(e) => setNewName(e.target.value)} />
          <button onClick={() => submitUpdateTodo()}>Save</button>
        </>
      ) : (
        <>
          <span>
            {item.name} ({item.description})
          </span>
          <button onClick={() => initUpdateTodo(item)}>Update</button>
        </>
      )}
      <button onClick={() => onDeleteTodo(item.id)}>Delete</button>
    </li>
  );
};

export default ListItems;
