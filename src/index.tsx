import React from "react";
import ReactDOM from "react-dom";
import Amplify from "aws-amplify";
import { AmplifyProvider } from "@aws-amplify/ui-react";

import "./index.css";
import awsExports from "./aws-exports";
import App from "./App";

Amplify.configure(awsExports);

ReactDOM.render(
  <React.StrictMode>
    <AmplifyProvider>
      <App />
    </AmplifyProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
