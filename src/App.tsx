import React from "react";
import { Authenticator as AmplifyAuthenticator } from "@aws-amplify/ui-react";
import "@aws-amplify/ui-react/styles.css";

import AddItem from "./components/AddItem";
import ListItems from "./components/ListItem";

function App() {
  return (
    <AmplifyAuthenticator>
      {({ user, signOut }) => (
        <div>
          <h1>Hello, {user.username}</h1>
          <br />
          <br />
          <br />
          <AddItem /> <br />
          listitems
          <br />
          <br />
          <ListItems />
          <br />
          <button onClick={signOut}>Sign out</button>
        </div>
      )}
    </AmplifyAuthenticator>
  );
}

export default App;
